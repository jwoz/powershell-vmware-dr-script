<#
	.SYNOPSIS
		A script to perform DR related actions against vCenter
	.DESCRIPTION
		This Script was written to perform semi-automated disaster recovery actions
	.PARAMETER Action
		Specify "get-datastores" to return the list of DR-related datastores in the specified vCenter. Useful to run when you are preparing for a DR test. You can use this list to help with importing LUNs at the Target Datacenter.
		Specify "shutdown" to shut down the DR-related VMs in the specified vCenter.
		Specify "poweron" to power on the DR-related VMs in the spefified vCenter.
		Specify "removevms" to unregister the DR-related VMs from the specified vCenter. You would do this after a DR test to clean up the Target Datacenter.
		Specify "importvms" to register any non-registered VMs located on any of the datastores connected the -hostToScan host.
		Specify "set-networking" to assign newly imported VMs to the proper port groups.  Along with this, you must specifc -portGroupFile with the path to a CSV containing the mappings. You can also specify -onlyVMs here.
		Specify "get-vms" to return a list of VMs, folder, datastore, and resource pool name. This requires a special powershell module however.
	.PARAMETER vCenters
		Specify one or more vCenters, e.g. -vCenters "datacenter1","datacenter2"
		Suggestion: Do not specify more than one vCenter when performing the "removevms" action
	.PARAMETER powerOnFirst
		This is a comma-separated list of VM names you want to power on before all the other DR-related VMs are powered on.
	.PARAMETER onlyVMs
		This is a comma-separated list of VM names you want to perform an action against.  Only these VMs will have actions performed against them and no others. Not all actions allow the use of this parameter.
	.PARAMETER hostToScan
		Use this parameter to tell the script to scan this host when importing VMS - use only with "importvms" action
	.PARAMETER portGroupFile
		For use with the "set-networking" action.  Use this parameter to specify a CSV file containing the port group mappings.  File format is two columns with column headers named "OldPortGroup" and "NewPortGroup"
	.PARAMETER Debug
		Specify the -Debug switch to enable debugging output
	.PARAMETER LogResults
		Specify the -LogResults swtich to enable logging of results to a file.  If you do not specify a file (using the -LogFile paramter) the default log will be yourcomputername-scriptname.log
	.PARAMETER LogFile
		Specify what file to log to when Log Results is enabled.  By default the file name is yourcomputername-scriptname.log in the same directory this script is in.
    .EXAMPLE
		Runs the DR-Actions script to get a list of datastores that cotain DR-related VMs in a vCenter instance named "vcenter01".
		.\DR-actions.ps1 -Action get-datastores -vCenters vcenter01
		  OR
		.\DR-actions.ps1 get-datastores vcenter01
	.EXAMPLE
		Shuts down the DR-related VMs in the vCenter named "vcenter01"
		.\DR-actions.ps1 shutdown vcenter01
		OR
		Will only shutdown the following VMS vm01,vm02,vm99 in the vCenter named "vcenter01"
		.\DR-actions.ps1 shutdown vcenter01 -onlyVMs vm01,vm02,vm99
	.EXAMPLE
		Powers on all DR-related VMs in the vCenter named "vcenter01", but powers on vm01 and vm02 first.
		.\DR-actions.ps1 poweron vcenter01 -powerOnFirst vm01,vm02
		or
		Power on vm01 and vm02 FIRST, then only power on vm03 and vm04 (no other VMs will be powered on).
		.\DR-actions.ps1 poweron vcetner01 -powerOnFirst vm01,vm02 -onlyVMs vm03,vm04
	.EXAMPLE
		Removes the DR-related VMs from the vCenter named "vcenter01".
		.\DR-actions.ps1 removevms vcenter01
		Removes the Non-DR related VMs that were imported by virtue of being on the same LUN.  Will remove any VMs who's Notes field begins with the text you specify as the -onlyVMs parameter.
		.\DR-actions.ps1 removevms vcenter01 -onlyVMs NDR
	.NOTES
		Version: 1.8
		Author: Joe Wozniak <joewoz@gmail.comm>
		Req: PowerCLI version 5.1+ and VM folder created containing the VMs you wish to perform DR actions against.
		Req: For the "get-vms" action, you need the get-folderpath powerscli module.  Run the script with the get-vms action to get more info.
#>
param (
	[string]$Action, 
	[string[]]$vCenters="wdc-vcenter",
	[string[]]$powerOnFirst=$null,
	[string[]]$onlyVMs=$null,
	[string]$portGroupFile=$null,
	[string]$hostToScan=$null,
	[switch]$Debug=$False, 
	[switch]$LogResults=$True, 
	[string]$LogFile="$env:Computername-DR-Actions.log"
)

$Version=1.8
$ScriptName = "" #Null-initialize global variable to hold script name
$DRFolderName = "Floating Datacenter"  #CHANGE THIS!!!!!!!!!!!!!!!! to the name of your DR folder in vCenter.

# +-----------------------------------------------------------------------
# | FUNCTIONS
# +-----------------------------------------------------------------------

function DisplayHeader {
	##------------------------------------------------------------------------
	## Function: DisplayHeader
	##------------------------------------------------------------------------
	"---------------------------------------------------------" 
	"-  Script: $ScriptName" 
	"-  Version: $Version" 
	"---------------------------------------------------------" 
	If ($LogResults) {
		OutLog "---------------------------------------------------------"  
		OutLog "-  Script: $ScriptName" 
		OutLog "-  Version: $Version" 
		OutLog "---------------------------------------------------------" 
	}
}

function OutLog {
	##-----------------------------------------------------------------------
	## Function: OutLog
	## Purpose: Used to write output to a log file
	##-----------------------------------------------------------------------
	param ([string]$strOut, [bool]$blnNoDate)
	$strDateTime = get-date -Format "G"
	If (-not ($blnNoDate)) {
		$strOutPut = ($strDateTime+ ": " + $strOut)
	} else {
		$strOutPut = $strOut
	}
	If ($LogFile -and $LogResults) {"$strOutPut" | out-file -filepath $LogFile -append}
}

function OutLn {
	##-----------------------------------------------------------------------
	## Function: OutLn
	## Purpose: Used to write output to the screen
	##-----------------------------------------------------------------------
	param ([string]$strOut, [string]$strType)

	If ($strOut -match "ERR:") {
		write-host $strOut -ForegroundColor RED -BackgroundColor WHITE
	} elseIf ($strOut -match "WARN:") {
		write-host $strOut -ForegroundColor CYAN -BackgroundColor WHITE
	} elseIf ($strOut -match "WARNING:") {
		write-host $strOut -ForegroundColor CYAN -BackgroundColor WHITE
	} elseIf ($strOut -match "INFO:") {
		write-host $strOut -ForegroundColor BLUE -BackgroundColor WHITE
	} elseIf ($strOut -match "DEBUG:") {
		write-host $strOut -ForegroundColor DARKGREEN -BackgroundColor WHITE
	} elseIf ($strOut -match "OTH:") {
		write-host $strOut -ForegroundColor DARKBLUE -BackgroundColor WHITE
	} Else {
		write-host $strOut
	}
	If ($LogResults) {OutLog $strOut}
}

function Usage{
	get-help .\$scriptname -full
    return
}

function Register-VMX {  
  param($entityName = $null,$dsNames = $null,$template = $false,$ignore = $null,$checkNFS = $false,$whatif=$false)  

  function Get-Usage{  
    Write-Host "Parameters incorrect" -ForegroundColor red  
    Write-Host "Register-VMX -entityName  -dsNames [,...]" 
    Write-Host "entityName   : a cluster-, datacenter or ESX hostname (not together with -dsNames)" 
    Write-Host "dsNames      : one or more datastorename names (not together with -entityName)" 
    Write-Host "ignore       : names of folders that shouldn't be checked" 
    Write-Host "template     : register guests ($false)or templates ($true) - default : $false" 
    Write-Host "checkNFS     : include NFS datastores - default : $false" 
    Write-Host "whatif       : when $true will only list and not execute - default : $false" 
  }  

  if(($entityName -ne $null -and $dsNames -ne $null) -or ($entityName -eq $null -and $dsNames -eq $null)){  
    Get-Usage 
    break  
  }  

  if($dsNames -eq $null){  

    switch((Get-Inventory -Name $entityName).GetType().Name.Replace("Wrapper","")){  
      "Cluster"{  
        $dsNames = Get-Cluster -Name $entityName | Get-VMHost | Get-Datastore | where {$_.Type -eq "VMFS" -or $checkNFS} | % {$_.Name}  
      }  
      "Datacenter"{  
        $dsNames = Get-Datacenter -Name $entityName | Get-Datastore | where {$_.Type -eq "VMFS" -or $checkNFS} | % {$_.Name}  
      }  
      "VMHost"{  
        $dsNames = Get-VMHost -Name $entityName | Get-Datastore | where {$_.Type -eq "VMFS" -or $checkNFS} | % {$_.Name}  
      }  

      Default{  
        Get-Usage 
        exit  
      }  
    }  
  }  

  else{  
    $dsNames = Get-Datastore -Name $dsNames | where {$_.Type -eq "VMFS" -or $checkNFS} | Select -Unique | % {$_.Name}  
  }  

  $dsNames = $dsNames | Sort-Object 
  $pattern = "*.vmx" 
  if($template){  
    $pattern = "*.vmtx" 
  }  

  foreach($dsName in $dsNames){  
    outln "Scanning datastore: $dsName"
    $ds = Get-Datastore $dsName | Select -Unique | Get-View 
    $dsBrowser = Get-View $ds.Browser  
    $dc = Get-View $ds.Parent  
    while($dc.MoRef.Type -ne "Datacenter"){  
      $dc = Get-View $dc.Parent  
    }  
    #$tgtfolder = Get-View $dc.VmFolder  #On 8/26/2014 this was commented out by JW to register VMs into the "Floating Datacenter" folder
	$tgtfolder = Get-View (get-folder $DRFolderName) #This line replaces the previous line that only registered the VMs into the root
	
    $esx = Get-View $ds.Host[0].Key  
    $pool = Get-View (Get-View $esx.Parent).ResourcePool  

    $vms = @()  
    foreach($vmImpl in $ds.Vm){  
      $vm = Get-View $vmImpl 
      $vms += $vm.Config.Files.VmPathName  
    }  
    $datastorepath = "[" + $ds.Name + "]" 
    $searchspec = New-Object VMware.Vim.HostDatastoreBrowserSearchSpec  
    $searchspec.MatchPattern = $pattern 
    $taskMoRef = $dsBrowser.SearchDatastoreSubFolders_Task($datastorePath, $searchSpec)  
    $task = Get-View $taskMoRef 
    while ("running","queued" -contains $task.Info.State){  
      $task.UpdateViewData("Info.State")  
    }  
    $task.UpdateViewData("Info.Result")  
    foreach ($folder in $task.Info.Result){  
      if(!($ignore -and (&{$res = $false; $folder.FolderPath.Split("]")[1].Trim(" /").Split("/") | %{$res = $res -or ($ignore -contains $_)}; $res}))){  
        $found = $FALSE 
        if($folder.file -ne $null){  
          foreach($vmx in $vms){  
            if(($folder.FolderPath + $folder.File[0].Path) -eq $vmx){  
              $found = $TRUE 
            }  
          }  
          if (-not $found){  
            if($folder.FolderPath[-1] -ne "/"){$folder.FolderPath += "/"}  
            $vmx = $folder.FolderPath + $folder.File[0].Path  
            if($template){  
              $params = @($vmx,$null,$true,$null,$esx.MoRef)  
            }  
            else{  
              $params = @($vmx,$null,$false,$pool.MoRef,$null)  
            }  
            if(!$whatif){  
              $taskMoRef = $tgtfolder.GetType().GetMethod("RegisterVM_Task").Invoke($tgtfolder, $params)  
              outln ("`t" + $vmx + " registered")
            }  
            else{  
              Write-Host "`t" $vmx " registered" -NoNewline; Write-Host -ForegroundColor blue -BackgroundColor white " ==> What If" 
            }  
          }  
        }  
      }  
    }  
    
  }  
}  


# +-----------------------------------------------------------------------
# | MAIN ROUTINE                                                                                         
# +-----------------------------------------------------------------------
function Main()
{
	#Remove old logs:
	#If (Test-Path $LogFile) {Remove-Item -path $LogFile}
	
	#Set Script Name (Only PowerShell v3 or later)
	if ($host.version.major -gt 2){$ScriptName = $MyInvocation.ScriptName | Split-Path -leaf}

	#set console colours
	$objHost = (get-host).UI.RawUI
	$objHost.BackgroundColor = "WHITE"
	$objHost.foregroundcolor = "DARKBLUE"

	#cls

	if ($Action.ToUpper() -ne "GET-VMS") {DisplayHeader}

	$strTitle = "SCRIPT: $ScriptName vCenters: $vCenters"
	$host.ui.rawui.windowtitle = $strTitle	

	If (($Action -eq $null) -or ($Action -eq '')) {
		Usage
		return
	}

	$strAction = $Action.ToUpper()

	ForEach ($vc in $vCenters) {
		OutLn "INFO: Connecting to $vc..."
		$VIServer = connect-viserver $vc
		If ($VIServer.IsConnected -ne $true){
			OutLn "ERR: Unable to connect to $vc, exiting."
			return
		}else{
			switch ($strAction) {
				"GET-DATASTORES" {			
					outln "Getting list of datastores in $vc, this may take a bit..."
					
					If ($debug){outln "Getting Virtual Machine objects that are part of the vCenter folder named $DRFolderName"}
					$allvms = get-vm -location (get-folder $DRFolderName)
					If ($allvms -eq $null){outln "ERR: No VMs are currently selected in $DRFolderName";return}
					If ($debug){outln "Getting Associated Datastores"}
					$dsarray = Get-Datastore -relatedobject $allvms
					$report = @()
					If ($debug){outln "Getting Associated Clusters"}
					ForEach($ds in $dsarray)
					{
						$row = "" | Select Datastore,Cluster,canonicalName
						$row.Datastore = $ds.Name
						$row.Cluster = (get-vmhost -ID $ds.ExtensionData.Host.key | get-cluster).Name
						$row.canonicalName = $ds.extensiondata.info.vmfs.extent[0].diskname #only gets the naa number from the first extent, if you have more than one, shame on you!
						$report += $row
					}
					OutLn "Below is the list of datastores for the currently connected vCenter that should be replicated to your Target Datacenter(s):"
					$report | sort Datastore
					$report | %{OutLog ($_.Datastore + " " + $_.canonicalName + " on cluster " + $_.Cluster)}
					
				}
				"SHUTDOWN" {
					
					if ($onlyVMs -ne $null){
						$Global:allvms = get-vm $onlyVMs
						outln "Will begin shutting down [$onlyVMs]..."
					}else{
						$Global:allvms = get-vm -location (get-folder $DRFolderName)
						outln "Will begin shutting down all VMs in [$DRFolderName] on $vc..."
						$choice = ""
						while ($choice -notmatch "[y|n]"){$choice = read-host "Are you sure you want to shut down all the DR-related VMs? (y/n)"}
						if ($choice -eq "n"){return}
					}
					
					outln "Performing poweroff / shutdown to the following VMS:"
					$allvms | Select Name,PowerState
					$allvms | %{OutLog $_.Name}
					$willshutdown = $allvms | where {$_.guest.extensiondata.toolsRunningStatus -eq "guestToolsRunning"}
					$willpoweroff = $allvms | where {$_.guest.extensiondata.toolsRunningStatus -eq "guestToolsNotRunning"}
					$tmpGuests = if($willshutdown -ne $null){$willshutdown | Shutdown-VmGuest -Confirm:$false}
					$tmpTask2 = if($willpoweroff -ne $null){stop-vm -vm $willpoweroff -confirm:$false -RunAsync}
					outln "VM Power off / shutdown complete, please verify in vCenter that all VMs are in the powered off state."
				}
				"REMOVEVMS" {				
					
					if ($onlyVMs -ne $null){
						$searchText = $onlyVMs.Trim() + "*"
						outln "Will begin removing all the VMs containing '$searchText' in the Notes field."
						
						$Global:vmsToRemove = get-vm | where {$_.Notes -like $searchText}
						
					}else{
						outln "Will begin removing all the VMs in [$DRFolderName] from $vc..."
						outln "Make sure all the VMs in [$DRFolderName] are shut down before you do this."						
						$Global:vmsToRemove = get-vm -location (get-folder $DRFolderName) 
					}
					if ($vmsToRemove -eq $null){outln "No VMs found, doing nothing";return}
					$choice = ""
					while ($choice -notmatch "[y|n]"){$choice = read-host "Are you sure you want to continue with this operation? (y/n)"}
					if ($choice -eq "n"){return}
					
					
					outln "The following VMS will be removed:"
					$vmsToRemove | Select Name,PowerState
					$vmsToRemove | %{OutLog $_.Name}
					$vmsToRemove | Remove-VM -Confirm:$false -RunAsync
					outln "VM Removal task has been started. Monitor vCenter for the status."
				}
				"POWERON" {
					if ($onlyVMs -eq $null){
						$choice = ""
						while ($choice -notmatch "[y|n]"){$choice = read-host "Are you sure you want to power on all the DR-related VMs? (y/n)"}
						if ($choice -eq "n"){return}
					}
					if ($powerOnFirst -ne $null){
						outln "Powering on the following VMs first: $powerOnFirst"
						get-vm $powerOnFirst | Start-VM -Confirm:$false -RunAsync
						Start-Sleep 5
						Get-VMQuestion | Set-VMQuestion -Option "I moved it" -Confirm:$false
						outln "Waiting 30 seconds for VMs [$powerOnFirst] to boot up."
						Start-Sleep 30
						outln "Now powering on the remaining VMs"
					}
					if ($onlyVMs -ne $null){
						outln "Powering on ONLY the following VMs: $onlyVMs"
						get-vm $onlyVMs | Start-VM -Confirm:$false -RunAsync						
					}else{
						outln "Will begin powering on all the DR-related VMs in $vc..."
						$willpoweron = get-vm -location (get-folder $DRFolderName) 
						outln "The following VMs will be powered on:"
						$willpoweron | Select Name,PowerState
						$willpoweron | %{OutLog $_.Name}
						$willpoweron | Start-VM -Confirm:$false -RunAsync
					}
					
					outln "Waiting 10 seconds for any VM questions to come up."
					Start-Sleep 10
					outln "Now answering any VM questions..."
					Get-VMQuestion | Set-VMQuestion -Option "I moved it" -Confirm:$false
					
					outln "VM Power On complete"
				}
				"IMPORTVMS" {			
					outln "Will begin registering all the VMs in folder [$DRFolderName] in $vc..."
					$choice = ""
					while ($choice -notmatch "[y|n]"){$choice = read-host "Are you sure you want to import all the DR-related VMs? (y/n)"}
					if ($choice -eq "n"){return}
					
					if ($hostToScan -eq $null){
						outln "ERR: Must specify -hostToScan parameter"
					}else{
						Register-VMX -entityName $hostToScan
						outln "VM Import complete"
					}
				}
				"SET-NETWORKING" {					
					if (test-path $portGroupFile){
						outln "Will begin updating network interfaces... Note: This Function is designed to be run against VMs that are powered off."
						$choice = ""
						while ($choice -notmatch "[y|n]"){$choice = read-host "Are you sure you want to begin assigning portgroups? (y/n)"}
						if ($choice -eq "n"){return}	
						
						Import-Csv $portGroupFile -UseCulture | %{
							#Rename Backup Interfaces
							$OldPortGroup = $_.OldPortGroup
							$NewPortGroup = $_.NewPortGroup
							outln "Now finding all NICs assigned [$OldPortGroup] and will assign them [$NewPortGroup]"
							if ($onlyVMs -ne $null){
								outln "Performing portgroup re-assign against only the following vms: $onlyVMs"
								$nics = Get-VM $onlyVMs | Get-NetworkAdapter | Where {$_.NetworkName -eq $OldPortGroup}
							}else{
								$nics = Get-VM -location (get-folder $DRFolderName) | Get-NetworkAdapter | Where {$_.NetworkName -eq $OldPortGroup}
							}
							outln ("Found " + $nics.count + " interfaces that will be modified.")
							if ($nics.count -gt 0){$tmpTask = $nics | Set-NetworkAdapter -PortGroup $NewPortGroup -confirm:$False -RunAsync}
							#$nics | Set-NetworkAdapter -StartConnected $True -confirm:$False -RunAsync  #This should not be needed
						}
					}else{
						outln "ERR: Must specify the -portGroupFile parameter using a valid file name/path."
					}
				}
				"GET-VMS" {
					outln "ACTION: get-vms - Now collecting information..."
					if ((get-module Get-FolderPath) -eq $null){
						outln "ERR: Must have Get-FolderPath PowerShell Module available here --> http://www.lucd.info/2010/10/21/get-the-folderpath/"
						outln "ERR: Copy the script, save it to a .psm1 file.  Add a 'load-module' line to your PS profile in MyDocs\WindowsPowerShell\Microsoft.PowerShell_profile.PS1"
						return
					}
					#Gets list of vms in the $DRFolderName folder					
					$drvms = get-vm -location (get-folder $DRFolderName)
					$report = @()
					foreach ($vm in $drvms){
						$entry = "" | Select Name,Folder,DataStore,ResourcePool
						$entry.Name = $vm.Name
						$entry.Folder = (get-folderpath -folder $vm.folder).Path -join(",")
						$entry.DataStore = (get-datastore -id $vm.extensiondata.datastore).Name -join(",")
						$entry.ResourcePool = $vm.resourcepool.Name
						$report += $entry
					}
					outln "The following VMs are currently in the $DRFolderName folder and actionable with this script:"
					$report | %{OutLog ($_.Name + "," + $_.Folder + "," + $_.DataStore + "," + $_.ResourcePool)}
					return $report
				}
				default {
					Usage
					return
				}
			}
		  
		}
		If ($debug){OutLn "DEBUG: Disconnecting all vCenter sessions..."}
		Disconnect-VIServer -Server * -Force -Confirm:$false
	}
}

#You can use $erroractionpreference to control what happens when a script error occurs. If you want the script to keep executing even when errors occur, uncomment the below line.
#$erroractionpreference = "SilentlyContinue"

#Call main and get the script running

Main